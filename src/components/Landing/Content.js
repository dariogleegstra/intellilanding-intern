import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';

import Element from './Element';


const TOOLS = [
    {
        title: 'Instapage',
        image: 'https://gdm-catalog-fmapi-prod.imgix.net/ProductLogo/b45050f7-ef58-4201-9f4c-87c40bf423c7.png?auto=format&q=50&fit=fill',
        description: 'The premium landing page...',
        url: 'https://instapage.com/'
    },
    {
        title: 'RDStation',
        image: 'https://yt3.ggpht.com/ytc/AKedOLSAVNWIutC-M8m_oACuAVvIjHzn08LkMX_n5N0qeg=s900-c-k-c0x00ffffff-no-rj',
        description: 'RD Station is a digital marketing...',
        url: 'https://www.rdstation.com'
    },
    {
        title: 'Google Services',
        image: 'https://i.blogs.es/0c6004/alternativasappsgoogle/500_333.jpg',
        description: 'Gmail, google...',
        url: 'https://www.google.com'
    },
    {
        title: 'ClickFunnels',
        image: 'https://media-exp1.licdn.com/dms/image/C560BAQGNfavRHaBJ1g/company-logo_200_200/0/1609859593647?e=2159024400&v=beta&t=rDjnGewlLcMfiR0cPAK7FXe8Xox2Nf1fxahwJwqPyvI',
        description: 'The Clickfunnel landing page...',
        url: 'https://www.clickfunnels.com'
    }
]; 

const StyledGrid = styled(Grid)(({ theme }) => ({
    marginBottom: "3%",
    margin: "0 auto",
    width: "100%",
}));

const boxStyles = {
    flexGrow: 1,
    backgroundColor: "#e5e5d4"
}

const Content = () => (
    <Box sx={ boxStyles }>
        <StyledGrid
            container
            spacing={10}
        >
            {TOOLS.map(tool => (
                <Grid item xs={4}>
                    <Element toolData={tool} />
                </Grid>
            ))}                
        </StyledGrid>
    </Box>
);

export default Content;