import React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import { styled } from '@mui/material/styles';

const StyledAppBar = styled(AppBar)(( { theme }) => ({
    backgroundColor: "darkblue"
}));

const StyledToolbar = styled(Toolbar)(( { theme }) => ({
    textAlign: "center",
    display: "block",
    margin: "0 auto",
}))

const Header = () => <>
    <Box sx={{ flexGrow: 1 }}>
      <StyledAppBar position="static">
        <StyledToolbar>
            
                <img 
                    src="/assets/images/intellisenseLogo.png"
                    alt='Intellisense logo'
                    height='80px'
                />
        </StyledToolbar>
      </StyledAppBar>
    </Box>
</>;

export default Header;