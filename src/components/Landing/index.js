import React from 'react';

import Header from './Header';
import Content from './Content';


const Landing = () => 
    <>
        <Header/>
        <Content/>
    </>;

export default Landing;