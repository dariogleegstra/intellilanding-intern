import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';



const StyledCard = styled(Card)(({ theme }) => ({
  cursor: 'pointer',
  '&:hover': {
    background: "lightgrey",
  },
}));


const redirect = url => {
  window.open(url, '_blank');
}

const Element = ({ toolData }) => <>
    <StyledCard sx={{ maxWidth: 345 }} onClick={() => redirect(toolData.url)}>
      <CardMedia
        component="img"
        height="140"
        image={toolData.image}
        alt={toolData.title}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {toolData.title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {toolData.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Share</Button>
        <Button size="small">Learn More</Button>
      </CardActions>
    </StyledCard>
</>;

export default Element;